﻿using System;

namespace HelloYou
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input name: ");
            string name = Console.ReadLine();
            int count = 0;
            char lastChar = 'c';
            foreach(char c in name)
            {
                count++;
                lastChar = c;
            }
            Console.WriteLine("Hello, {0}, your name is {1} characters long and ends on {2}", name, count, lastChar);
        }
    }
}
